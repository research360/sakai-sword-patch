This patch represents the results of an abortive attempt to extend
[Sakai CLE][] to enable straightforward deposit of files stored in the
Resources (file-sharing) tool to a data repository using the [SWORD][]
protocol.

The work is described in the report
[Research360: Sakai-SWORD2 Integration Development Report][report].

[Sakai CLE]: http://sakaiproject.org/sakai-cle
[SWORD]: http://swordapp.org/
[report]: http://opus.bath.ac.uk/35540/

Applying the patch to a Sakai CLE source tree will currently result in
code that does not compile correctly, but it is published in the hope
that it will be of use to the community, as a starting point for
further work or simply as a suggestion for how such a feature might be
added in the future.

## Using the patch

The patch was produced against the 2.8.x maintenance branch of Sakai
CLE, and should apply with few problems to any release in the 2.8
series.

## Further development

If you build on this work, or simply produce something similar that
works, we'd love to hear from you. Please drop us a line at
<research-data@bath.ac.uk>.

## Copyright & licensing

Copyright 2013 University of Bath. Licensed under the Educational
Community License, Version 2.0 (the "License"); you may not use this
file except in compliance with the License. You may obtain a copy of
the License at

<http://opensource.org/licenses/ecl2.php>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
implied. See the License for the specific language governing
permissions and limitations under the License.
